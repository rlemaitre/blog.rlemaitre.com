---
title: "Setup a Hugo Blog With Asciidoc on Gitlab Pages"
date: 2019-12-20T15:18:11+01:00
draft: true

categories:
    - ""
tags:
    - "howto"
    - "hugo"
    - "asciidoc"
    - "gitlab"
---

:toc:

It's been a long time I wanted to have a blog.
I had several options to do this:

- Write posts on https://medium.com/@rlemaitre[Medium]
- Write posts on https://dev.to/rlemaitre[dev.to]
- Host a blog using wordpress


2019-12-20
